﻿#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>

#define SIZE 1024

Server::Server()
{
	this->isEmpty = true;
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	this->_usersLen = 0;
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...)
	{}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}

void Server::insertMsg(SOCKET clientSocket, std::string userName)
{
	std::unique_lock<std::mutex> lockMsg(msgMtx, std::defer_lock);
	Helper::send_update_message_to_client(clientSocket, "", "", this->_userStr); // update the num of users that active.
	std::string data = Helper::getStringPartFromSocket(clientSocket, SIZE);
	lockMsg.lock();
	this->_msg.push(data);
	lockMsg.unlock();
	cv.notify_one();
}

void Server::popMsg(std::string userName)
{
	std::unique_lock<std::mutex> lockMsg(msgMtx, std::defer_lock);
	while (true)
	{
		lockMsg.lock();
		cv.wait(lockMsg);
		lockMsg.unlock();

		lockMsg.lock();
		isEmpty = this->_msg.empty();
		lockMsg.unlock();

		std::string currMsg = this->_msg.front();
		std::string temp = currMsg.substr(0, 2); // trying to avoid the data from the msg would be trash.

		if (this->_UsersSocks.size() != 0 && !isEmpty && currMsg != "" && temp != "ÍÍ") // checks if there are users in the users list.
		{
			if (temp != "ÝÝ")
			{
				std::string currMsg = this->_msg.front();
				this->_msg.pop();
				temp = currMsg.substr(0, 2);
				int usernameLen = std::stoi(temp); // get length of the username from the msg.
				std::string otherUsername = currMsg.substr(2, usernameLen); // get the username.
				temp = currMsg.substr(usernameLen + 2, 5);
				int dataLen = std::stoi(temp); // get the length of the data from the msg.
				std::string dataToSend = currMsg.substr(usernameLen + 7, dataLen); // get the data.

				std::map<std::string, SOCKET>::iterator it;
				lockMsg.lock();
				it = this->_UsersSocks.find(otherUsername); // check if username exist.
				lockMsg.unlock();

				if (it != this->_UsersSocks.end() && dataToSend != "")
				{
					SOCKET otherSock = it->second;
					std::string fileName = writeToFile(userName, otherUsername, dataToSend);
					std::cout << "Wrote to the file!" << std::endl;
					std::string fileData = readFromFile(fileName);
					std::cout << "red the data from the file!" << std::endl;
					checkUsersList();
					Helper::send_update_message_to_client(otherSock, fileData, otherUsername, this->_userStr); // sending the msg to the user and update the conversation at the other user.
					std::cout << dataToSend << std::endl;
					std::cout << "MSG has been send!" << std::endl;
				}
			}
			else
			{
				lockMsg.lock();
				this->_msg.pop();
				lockMsg.unlock();
			}
		}
			}
}

std::string Server::writeToFile(std::string fromUsername, std::string toUserName, std::string dataToSend)
{
	std::ofstream outfile;
	std::string fileName;
	int firstSize = fromUsername.size();
	int secondSize = toUserName.size();
	int name = 0;
	int name2 = 0;
	for (int i = 0; i < firstSize || i < secondSize; i++)
	{
		name = fromUsername[i];
		name2 = toUserName[i];
		if (name < name2)
		{
			fileName = fromUsername + "&" + toUserName + ".txt";
			break;
		}
		else if (name > name2)
		{
			fileName = toUserName + "&" + fromUsername + ".txt";
			break;
		}
	}
	outfile.open(fileName, std::ios_base::app); // append instead of overwrite
	std::string data = "&MAGSH_MESSAGE&&Author&" + fromUsername + "&DATA&" + dataToSend;
	outfile << data;
	outfile.close();

	return fileName;
}

std::string Server::readFromFile(std::string fileName)
{
	std::ifstream file(fileName);
	std::string data((std::istreambuf_iterator<char>(file)),(std::istreambuf_iterator<char>()));// https://stackoverflow.com/questions/2912520/read-file-contents-into-a-string-in-c
	file.close();

	return data;
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	std::string userNameSock = Helper::getUserName(client_socket);
	this->_UsersSocks.insert(std::pair<std::string, SOCKET>(userNameSock, client_socket));

	std::cout << "ADDED new client, " << userNameSock << " to clients list." << std::endl;

	checkUsersList();
	std::thread th(&Server::clientHandler, std::ref(*this), client_socket, userNameSock);
	th.detach();
	std::thread popTh(&Server::popMsg, std::ref(*this), userNameSock);
	popTh.detach();
}


void Server::clientHandler(SOCKET clientSocket, std::string userName)
{
	int code = 0;
	try
	{
		Helper::send_update_message_to_client(clientSocket, "", "", _userStr);
		while (code != MT_CLIENT_EXIT && code != MT_CLIENT_FINISH)
		{
			checkUsersList();
			code = Helper::getMessageTypeCode(clientSocket);
			this->insertMsg(clientSocket, userName);
		}
		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (std::exception& e)
	{
		this->_UsersSocks.erase(userName);
		this->checkUsersList();
		std::cout << "REMOVED, " << userName << " from clients list" << std::endl;
		closesocket(clientSocket);
	}
}

void Server::checkUsersList()
{
	std::string userStr;
	for (auto name = this->_UsersSocks.begin(); name != this->_UsersSocks.end(); name++)
	{
		if (name == this->_UsersSocks.begin())
			userStr += name->first;
		else
			userStr += "&" + name->first;
	}

	this->_userStr = userStr;
	this->_usersLen = userStr.size();
}
