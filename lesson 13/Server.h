#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <queue>
#include "Helper.h"
#include <mutex>
#include <condition_variable>
#include <fstream>

class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	
	std::mutex msgMtx;
	std::condition_variable cv;
	bool isEmpty;
	
	void insertMsg(SOCKET clientSocket, std::string userName); // receive msg from socket and insert to the queue.
	void popMsg(std::string userName); // sends the msg to the user and pop the msg from the queue.
	std::string writeToFile(std::string fromUsername, std::string toUserName, std::string dataToSend); // write the conversation data to the conversation file.
	std::string readFromFile(std::string fileName); // read the data from the file.

private:

	std::map<std::string, SOCKET> _UsersSocks;
	std::string _userStr;
	int _usersLen;
	std::queue<std::string> _msg;

	void accept();
	void clientHandler(SOCKET clientSocket, std::string userName);
	void checkUsersList();

	SOCKET _serverSocket;
};

